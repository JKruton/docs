# San Francisco DSA Documents
## Bylaws and Code of Conduct

## Generating HTML
You can generate HTML versions of these markdown documents with the included build script. You'll need [showdown](https://www.npmjs.com/package/showdown) and [markdown-splendor](https://www.npmjs.com/package/markdown-splendor)

## Maintaining the table of Contents
We currently use a [SublimeText 3 plugin called MarkdownTOC](https://github.com/naokazuterada/MarkdownTOC#quick-start) to maintain the Table of Contents section in each document.