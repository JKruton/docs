# DSA San Francisco Bylaws
###### Ratified April 26th, 2017 / Amended March 10th, 2021

<!-- MarkdownTOC autolink="true" autoanchor="true" bracket="round" -->

- [Article I. Name](#article-i-name)
- [Article II. Purpose](#article-ii-purpose)
    - [Section 1. Vision](#section-1-vision)
    - [Section 2. Mission](#section-2-mission)
    - [Section 3. Objectives](#section-3-objectives)
- [Article III. Membership](#article-iii-membership)
    - [Section 1. Qualifications](#section-1-qualifications)
    - [Section 2. Dues](#section-2-dues)
    - [Section 3. “Good Standing”](#section-3-good-standing)
    - [Section 4. “Active”](#section-4-active)
    - [Section 5. Suspension and Expulsion](#section-5-suspension-and-expulsion)
    - [Section 6. Sexual Harassment and Anti-Discrimination Policy](#section-6-sexual-harassment-and-anti-discrimination-policy)
- [Article IV. Steering Committee](#article-iv-steering-committee)
    - [Section 1. Members](#section-1-members)
    - [Section 2. Responsibilities](#section-2-responsibilities)
    - [Section 3. Quorum.](#section-3-quorum)
- [Article V. Officer Positions and Duties](#article-v-officer-positions-and-duties)
    - [Section 1. Definition](#section-1-definition)
    - [Section 2. Diversity Requirement](#section-2-chapter-officer-diversity)
    - [Section 3. Co-Chairs and Vice Chair](#section-3-co-chairs-and-vice-chair)
    - [Section 4. Secretary](#section-4-secretary)
    - [Section 5. Treasurer](#section-5-treasurer)
    - [Section 6. At-large members](#section-6-at-large-members)
    - [Section 7. Candidacy and Elections](#section-7-candidacy-and-elections)
    - [Section 8. Acting Officers](#section-8-acting-officers)
    - [Section 9. Removal of an Officer](#section-9-removal-of-an-officer)
- [Article VI. Treasury](#article-vi-treasury)
- [Article VII. Committees and Working Groups](#article-vii-committees-and-working-groups)
    - [Section 1. Definition and Purpose](#section-1-definition-and-purpose)
    - [Section 2. Creation](#section-2-creation)
    - [Section 3. Membership and Election of Co-Chairs](#section-3-membership-and-election-of-co-chairs)
    - [Section 4. Diversity](#section-4-committee-officer-diversity)
    - [Section 5. Duties and Responsibilities](#section-5-duties-and-responsibilities)
    - [Section 6. Expenditures](#section-6-expenditures)
    - [Section 7. Review, Modification, and Dissolution](#section-7-review-modification-and-dissolution)
    - [Section 8. Provisional Working Groups](#section-8-provisional-working-groups)
- [Article VIII. Youth Sections](#article-viii-youth-sections)
    - [Section 1. Branch Status](#section-1-branch-status)
- [Article IX. Organizing Body and Meetings](#article-ix-organizing-body-and-meetings)
    - [Section 1. Members](#section-1-members-1)
    - [Section 2. General Meetings](#section-2-general-meetings)
    - [Section 3. Special Meetings](#section-3-special-meetings)
    - [Section 4. Regular Meetings](#section-4-regular-meetings)
    - [Section 5. Voting](#section-5-voting)
    - [Section 6. Quorum](#section-6-quorum)
- [Article X. Endorsements, Event Sponsorships, and Online Voting](#article-x-endorsements-event-sponsorships-and-online-voting)
    - [Section 1. Formal Endorsements and Event Sponsorships](#section-1-formal-endorsements-and-event-sponsorships)
    - [Section 2. Online Voting](#section-2-online-voting)
    - [Section 3. Ban on Misrepresentation of Formal Endorsements](#section-3-ban-on-misrepresentation-of-formal-endorsements)
- [Article XI. National Delegates](#article-xi-national-delegates)
    - [Section 1. Selection of Delegates](#section-1-selection-of-delegates)
- [Article XII. Elections](#article-xii-elections)
    - [Section 1. Nominee Campaigns](#section-1-nominee-campaigns)
    - [Section 2. Process](#section-2-process)
    - [Section 3. Ranked Choice Voting](#section-3-ranked-choice-voting)
    - [Section 4. Ballot Count](#section-4-ballot-count)
- [Article XIII. Dissolution](#article-xiii-dissolution)
- [Article XIV. General Provisions](#article-xiv-general-provisions)
    - [Section 1. Interpretation](#section-1-interpretation)
    - [Section 2. Rules](#section-2-rules)
- [Article XV. Amendments](#article-xv-amendments)
    - [Section 1. Qualifying Proposals](#section-1-qualifying-proposals)
    - [Section 2. Introduction and Ratification](#section-2-introduction-and-ratification)
- [Article XVI. Grievance Officers](#article-xvi-grievance-officers)
    - [Section 1. Definition and Scope](#section-1-definition-and-scope)
    - [Section 2. Grievance Officer Role and Responsibilities](#section-2-grievance-officer-role-and-responsibilities)
        - [Disciplinary and Remedial Recommendations](#disciplinary-and-remedial-recommendations)
        - [Other Responsibilities](#other-responsibilities)
        - [Consensus and Recusal](#consensus-and-recusal)
    - [Section 3. Candidacy and Elections](#section-3-candidacy-and-elections)
    - [Section 4. Vacancies](#section-4-vacancies)
    - [Section 5. Removal of a GO](#section-5-removal-of-a-go)

<!-- /MarkdownTOC -->


<a name="article-i-name"></a>
<a id="article-i-name"></a>
# Article I. Name
The name of this organization shall be the Democratic Socialists of America, San Francisco (“DSA SF”), a local chapter (“Chapter”) of the Democratic Socialists of America (“DSA”). The name is taken from the city and county of San Francisco, California (“San Francisco”) that the Chapter will serve.

<a name="article-ii-purpose"></a>
<a id="article-ii-purpose"></a>
# Article II. Purpose
<a name="section-1-vision"></a>
<a id="section-1-vision"></a>
## Section 1. Vision
DSA SF’s vision is a United States that is liberated from the control and corruption of capitalism in our economy, society, and community. Instead of a political-economic system which benefits an elite few, we envision a social order based on democratic control of resources and production, economic planning, equitable distribution, and social and economic justice for all. We envision San Francisco as an exemplar of these values.

<a name="section-2-mission"></a>
<a id="section-2-mission"></a>
## Section 2. Mission
DSA SF’s mission is to organize the people of San Francisco in order to establish and exercise collective power that can fight capitalism, combat inequality, and bring about real socialist change.

<a name="section-3-objectives"></a>
<a id="section-3-objectives"></a>
## Section 3. Objectives
1. Build collective power that can stand up to capital.
2. Actively organize throughout San Francisco to exhibit solidarity, build stronger communities, and bring about real material change for the people.
3. Educate ourselves and the people of San Francisco on socialist critiques of capitalism and advocate for democratic socialist values, vision, and policy.
4. Act as a San Francisco hub for DSA National events, projects, and campaigns.
5. Partner with, contribute to, and support adjacent DSA Chapters in the San Francisco Bay Area, California.

<a name="article-iii-membership"></a>
<a id="article-iii-membership"></a>
# Article III. Membership

<a name="section-1-qualifications"></a>
<a id="section-1-qualifications"></a>
## Section 1. Qualifications
Membership shall be open to dues-paying members of the DSA whom reside in or hold employment in San Francisco or the larger San Francisco Bay Area and are not members of another DSA Chapter.

<a name="section-2-dues"></a>
<a id="section-2-dues"></a>
## Section 2. Dues
Annual dues are set by the DSA National and must be paid to the DSA National office. Voluntary member dues may be established on behalf of DSA SF, but their nonpayment may not restrict membership status or eligibility to serve on any committees or working groups.

<a name="section-3-good-standing"></a>
<a id="section-3-good-standing"></a>
## Section 3. “Good Standing”
Members are considered in “good standing” provided that they are considered in good standing with DSA National.

<a name="section-4-active"></a>
<a id="section-4-active"></a>
## Section 4. “Active”
Members are considered “active” provided they have attended two of the three most recent “Regular Meetings” or consistently participate in Working Groups or Committees, as testified by nomination by a chair of a Working Group or Committee they are in.

<a name="section-5-suspension-and-expulsion"></a>
<a id="section-5-suspension-and-expulsion"></a>
## Section 5. Suspension and Expulsion
The Steering Committee may expel or suspend a member by a two-thirds vote provided the member is found to be in substantial disagreement with the principles or policies of the organization; they consistently engage in undemocratic, disruptive behavior; they publicly misrepresent the formal endorsements or positions adopted by the Chapter pursuant to Article XII; or they are under the discipline of any self-defined democratic-centralist organization. Members facing expulsion or suspension must receive written notice of the charges against them and must be given a reasonable opportunity to provide a written response to the Steering Committee (which must be in addition to any opportunity they may have to provide a written response to the Grievance Officers) prior to the aforementioned vote. After charges to suspend or expel a member are are disposed of or dismissed, the Steering Committee shall report those charges and the results of the process to the membership as soon as reasonably possible. Reports shall be made in summary form to protect the privacy and identities of the involved individual(s), and shall identify how Steering Committee members voted and which were recused.

In addition, pending the outcome of an investigation, grievance, or disciplinary process, the Steering Committee by majority vote may temporarily suspend a member’s access to any chapter communication channels, social media, events, meetings, or other spaces if necessary to protect the safety of individuals or the chapter.

Any member of the Steering Committee who cannot reasonably be expected to fairly adjudicate an investigation, grievance, or disciplinary process (such as if they are the complainant or if their own behavior is at issue) shall automatically be recused. All voting thresholds shall be based on the number of non-recused Steering Committee members.

<a name="section-6-sexual-harassment-and-anti-discrimination-policy"></a>
<a id="section-6-sexual-harassment-and-anti-discrimination-policy"></a>
## Section 6. Sexual Harassment and Anti-Discrimination Policy
Sexual harassment and discrimination on the basis of such inappropriate grounds as age, disability, national origin, race, religion, gender or sexual identity or orientation are antithetical to the principles of democratic socialism and to the vision and mission of DSA SF. A member determined to have engaged in sexual harassment or inappropriate discrimination shall be deemed to be in substantial disagreement with the principles or policies of DSA SF and may be considered for suspension or expulsion under [Article III Section 5](#section-5-suspension-and-expulsion).

DSA SF may adopt a formal policy that defines sexual harassment and inappropriate discrimination for purposes of this section and/or prescribes procedures for resolving allegations of sexual harassment or inappropriate discrimination.

<a name="article-iv-steering-committee"></a>
<a id="article-iv-steering-committee"></a>
# Article IV. Steering Committee

<a name="section-1-members"></a>
<a id="section-1-members"></a>
## Section 1. Members
The Officers of DSA SF along with any at-large members and representatives of branches shall compose the Steering Committee.

<a name="section-2-responsibilities"></a>
<a id="section-2-responsibilities"></a>
## Section 2. Responsibilities
* The Steering Committee shall meet as a whole at least once per quarter (in person or by conference call). Those meetings shall be open to chapter members unless the Committee goes into executive session by two-thirds vote of the committee.
* The Steering Committee shall concern itself primarily with establishing program activities and proposing guidelines for consideration by the general membership.
* The Steering Committee shall be responsible for providing the organizational infrastructure required for DSA SF’s meetings and for members to accomplish projects and tasks within working groups and committees.
* The Steering Committee, working with relevant committees, must publish annual reports describing the planned projects and accomplishments of DSA SF to members 15 days prior to General Meeting.
* The Steering Committee is responsible for organizing efforts to satisfy DSA’s priorities within San Francisco, as determined by the general membership.
* The Steering Committee shall be responsible for presenting budgetary appropriations to the members at Regular Meetings, to be approved by a majority vote of members present.
* The Steering Committee shall be responsible for acting on the organization's behalf between Regular and General Meetings.

<a name="section-3-quorum"></a>
<a id="section-3-quorum"></a>
## Section 3. Quorum.
A quorum of three members or fifty percent, whichever is higher, is required for a valid meeting of the Steering Committee. A Steering Committee member may appoint a proxy who is not already a voting member of the Steering Committee to vote on the member’s behalf when necessary. Unless otherwise specified in the bylaws, all votes by the Steering Committee require a simple majority of votes cast.

<a name="article-v-officer-positions-and-duties"></a>
<a id="article-v-officer-positions-and-duties"></a>
# Article V. Officer Positions and Duties

<a name="section-1-definition"></a>
<a id="section-1-definition"></a>
## Section 1. Definition
The officers of DSA SF will be the two Co-Chairs, Vice Chair, Secretary, Treasurer and all at-large members. The Grievance Officers shall not be considered “Officers” of DSA SF for purposes of these bylaws. All officers shall perform the duties prescribed by these Bylaws and the standing rules defined throughout the lifetime of DSA SF. No officer may hold more than one officer position at a time. Officers will serve for one-year terms or until successors are elected.

<a name="section-2-chapter-officer-diversity"></a>
<a id="section-2-chapter-officer-diversity"></a>
## Section 2. Diversity Requirement
Three or more elected officers must be either a woman, transgender, non-binary, or any combination of these categories, determined solely by the officers’ own identification. Additionally, three or more elected officers must be Black, Indigenous, and/or People of Color (BIPOC), again determined solely by the officers’ own identification. If fewer than enough candidates stand for election to satisfy either or both of these requirements then those respective requirements shall be waived.

At least one co-chair must be either a woman, transgender, non-binary, or any combination of these categories, determined solely by that co-chair’s own identification. Additionally, at least one co-chair must be Black, Indigenous, and/or a Person of Color (BIPOC), again determined solely by the officers’ own identification. If fewer than enough candidates stand for election to satisfy either or both of these requirements, these requirements will be altered as described in [Section 7](#section-7-candidacy-and-elections).

<a name="section-3-co-chairs-and-vice-chair"></a>
<a id="section-3-co-chairs-and-vice-chair"></a>
## Section 3. Co-Chairs and Vice Chair
The Co-Chairs shall be the chief spokespeople of the organization. They shall work with the rest of the Steering Committee to facilitate all meetings of the organization as a whole, shall have responsibility for the overall management of the organization, and shall interpret the Constitution and Bylaws, subject to appeal as described in Article XIV.The Vice Chair shall act as deputy to the Co-Chairs and succeed to that position in the event that either Co-Chair resigns or otherwise is removed from office.

<a name="section-4-secretary"></a>
<a id="section-4-secretary"></a>
## Section 4. Secretary
The Secretary shall be responsible for maintaining accurate membership lists, keeping minutes and records of chapter meetings, and maintaining communication channels utilized by DSA SF. The Secretary shall also be responsible for notifying the Steering Committee and the membership of all meetings.

<a name="section-5-treasurer"></a>
<a id="section-5-treasurer"></a>
## Section 5. Treasurer
The Treasurer shall have custody of all funds of the organization and shall be responsible for the financial management of the organization. The Treasurer shall be responsible for presenting regular budgetary updates at Steering Committee meetings, as well as an end-of-year financial statement. The Treasurer shall be responsible for authorizing all expenditures in accordance with the wishes of the Steering Committee (in the case of expenditures over $300, prior approval must be sought from the Steering Committee), and shall organize dues collection, should DSA SF decide to collect dues.

<a name="section-6-at-large-members"></a>
<a id="section-6-at-large-members"></a>
## Section 6. At-large members
Elected Steering Committee members not selected for a Co-Chair, Vice Chair, Secretary, or Treasurer position are considered at-large members of the Steering Committee.

<a name="section-7-candidacy-and-elections"></a>
<a id="section-7-candidacy-and-elections"></a>
## Section 7. Candidacy and Elections
Candidates for the Steering Committee must be active members in good standing with DSA SF and not under the discipline of any self-defined democratic-centralist organization. Every candidate is simply running to be on the Steering Committee, not for any specific position.

Candidates must be nominated by five members in good standing, not including themselves, by no later than the last regular meeting before the election is to be held.

At the meeting where the election is held, every valid candidate is given equal time to speak. Voters then rank their choices for candidates. Elections for the Steering Committee will be held according to the rules set forth in Article XII.

Officers are determined as follows:

  1. In total, seven Steering Committee members shall be elected through the process described in [Article XII](#article-xii-elections). All terms shall run from June to June. The two new members added by this amendment shall be elected in a special election following the process set forth in Article XII by no later than the January 2019 regular meeting.

  2. The pair of two elected members of Steering Committee that together satisfy the [diversity requirements](#section-2-chapter-officer-diversity) with the lowest combined (added) ranking are the co-chairs. In case of a tie, the tied pair with the lowest overall ranking among them are the cochairs. If no two elected members of steering committee together satisfy either or both of the co-chair diversity requirements then each respective requirement that can’t be satisfied shall be waived.

  3. If the officer [diversity requirements of Section 2](#section-2-chapter-officer-diversity) are not met by the election of the seven Steering Committee members, the candidates who would fulfill the requirement but did not receive enough votes to be elected shall be added as additional elected at-large Steering Committee members, based on the next most number of votes received, until the diversity requirement is satisfied or there are no more qualifying candidates.

  4. The Vice Chair is selected from among the committee members by vote of the new Steering Committee. The Secretary & Treasurer should be selected from among the committee members. However, if (and only if) no members of the Steering Committee wish to take the roles, then the roles may be appointed from the general membership. Unelected members appointed in this fashion assume the title, duties, and authorities of that role and become non-voting members of the Steering Committee.

<a name="section-8-acting-officers"></a>
<a id="section-8-acting-officers"></a>
## Section 8. Acting Officers
Should an officer leave DSA SF, resign the position, or be removed from office, a special election must be held as soon as feasible to fill the remainder of that officer’s term unless the regular election for that position is already scheduled to be held within 90 days. The Steering Committee may, but is not required to, select an acting officer to hold the position until the election. If the chapter is not in compliance with the diversity requirement of Section 2 after the original officer’s departure, any acting officer selected must satisfy the diversity requirement, and any special election may be limited to candidates who would satisfy the diversity requirement at issue.

<a name="section-9-removal-of-an-officer"></a>
<a id="section-9-removal-of-an-officer"></a>
## Section 9. Removal of an Officer
An Officer may be removed from their position by a majority of votes cast at a Regular or Special meeting called for that purpose. A removal vote is triggered by a majority vote of the Steering Committee to recommend removal of the Officer, or by a petition of 10% of active membership or 15 active members, whichever is greater. The membership must be given at least two weeks notice of the upcoming removal vote, which must be held as soon as feasible after the vote is triggered.

<a name="article-vi-treasury"></a>
<a id="article-vi-treasury"></a>
# Article VI. Treasury
The treasury must be deposited in a local credit union; corporate banking institutions are forbidden unless no credit union is available. Local expenditures out of the treasury must prefer contracts with union labor. Corporate donations are forbidden to be accepted by the treasury.

<a name="article-vii-committees-and-working-groups"></a>
<a id="article-vii-committees-and-working-groups"></a>
# Article VII. Committees and Working Groups

<a name="section-1-definition-and-purpose"></a>
<a id="section-1-definition-and-purpose"></a>
## Section 1. Definition and Purpose
In order to more effectively organize to achieve its mission and objectives, DSA SF may establish committees and working groups. Committees may be established for the purpose of organizing around and/or performing specific functions for the chapter (e.g., design, marketing and communication, public outreach, recommending endorsements, etc.). Working groups may be established for the purpose of organizing action around political themes or projects (e.g., housing, immigration, feminism, reading groups, etc.) that fall under the purview of the DSA and/or DSA SF’s political platform.

<a name="section-2-creation"></a>
<a id="section-2-creation"></a>
## Section 2. Creation
Members who wish to form a committee or working group must submit a proposed charter that includes (1) a group name; (2); mission statement and objectives; (3) initial strategy for achieving the stated objectives; (4) list of at least five prospective members (including two who are designated as Co-Chairs) signed by those members; and (5) if it is anticipated that the proposed committee or working group will seek authorization to expend the chapter’s funds, a short explanation of the anticipated use of those funds and estimate of the amount to be spent.

Proposed charters for committees, but not working groups, must additionally include (6) a statement of whether the proposed committee is to be a standing committee of indefinite duration or a temporary committee to expire at a time certain or upon the occurrence of a specified event; (7) whether membership in the committee is to be open to all members in good standing or limited; and (8) if membership is to be limited, the number of members the committee shall have and the means by which members will be selected.

A proposed committee or working group shall be recognized and chartered with the approval of a majority of the votes cast at a Regular, Special, or General meeting. Charters for proposed committees and working groups must be published on the agenda or announced to the membership at least 15 days prior to the meeting.

<a name="section-3-membership-and-election-of-co-chairs"></a>
<a id="section-3-membership-and-election-of-co-chairs"></a>
## Section 3. Membership and Election of Co-Chairs
Membership in a committee shall be open to all DSA SF members in good standing unless the committee’s charter limits membership, in which case the terms of the charter shall control. Committees shall maintain an official membership list.

Membership in working groups shall be open to all DSA SF members in good standing. Working groups are encouraged to maintain an official membership list, but are only required to do so to maintain eligibility to make authorized expenditures under [Section 6](#section-6-expenditures), below.

The founders of a new committee or working group must designate two temporary co-chairs. Temporary co-chairs will serve once the group is chartered; an election for committee leadership must then be held at no later than the second meeting following ratification. It is recommended that committees use the same Single Transferable Voting system that DSA SF uses for chapter-wide elections, especially for contested committee elections. Ratified committees and working groups must elect two co-chairs and may also elect a vice-chair to serve as their deputy. Terms will last either 6 months or one year.

All co-chairs and vice chairs must be added to current leadership channels and listservs. All co-chairs and vice-chairs must voluntarily leave said channels and listservs after their terms are over if they are not re-elected.

In the interest of sharing leadership opportunities and elevating rank and file members, co-chairs of committees and working groups (including steering committee) cannot chair more than one committee or working group unless no other eligible members wish to run.

<a name="section-4-committee-officer-diversity"></a>
<a id="section-4-committee-officer-diversity"></a>
## Section 4. Diversity

At least 50% of each committee’s co-chairs, vice chairs, and other elected officers determined by the committee should be either a woman, transgender, non-binary, or any combination of these categories, determined solely by that officer’s own identification. Additionally at least 50% of each committee’s co-chairs, vice chairs, and other elected officers determined by the committee should be Black, Indigenous, and/or People of Color (BIPOC). If either of these conditions are not met then the officers elected by the next election for any of these offices shall satisfy all of the unmet conditions, unless no such candidates stand for election, in which case the officers shall satisfy one of the unmet conditions, unless no such candidates stand for election, in which case this requirement is waived.

<a name="section-5-duties-and-responsibilities"></a>
<a id="section-5-duties-and-responsibilities"></a>
## Section 5. Duties and Responsibilities
The members of a committee or working group are responsible for executing their respective body’s strategy in fulfillment of its objectives. Co-Chairs are responsible for organizing their respective body’s members, running meetings, acting as liaisons to the Steering Committee, and serving as the body’s points of contact to the DSA SF membership.

Committees must keep official records of meeting dates, attendance, and meeting notes, which must be made available to the DSA SF membership. Working groups are encouraged to maintain these records, but are only required to do so to maintain eligibility to make authorized expenditures under [Section 6](#section-6-expenditures), below.

<a name="section-6-expenditures"></a>
<a id="section-6-expenditures"></a>
## Section 6. Expenditures
Committees, as well as working groups that maintain an official membership list and official records of meeting dates, attendance, and meeting notes, may seek authorization to expend the organization’s funds if necessary to accomplish their objectives. All requests for authorization must be presented to the Treasurer and Steering Committee and must be approved by the Steering Committee. A committee or working group shall maintain records of all authorized expenditures made using the organization’s funds and shall provide these records to the Treasurer.

<a name="section-7-review-modification-and-dissolution"></a>
<a id="section-7-review-modification-and-dissolution"></a>
## Section 7. Review, Modification, and Dissolution
A committee’s or working group’s activities shall be reviewed by the Steering Committee at the Regular Steering Committee meetings. Committees and working groups shall report on their activities to the DSA SF membership at one Regular meeting each quarter or more frequently as necessary.

Existing committees and working groups may amend their charters with the approval of a majority of the votes cast at a Regular, Special, or General meeting. Proposed amendments to a committee’s or working group’s charter must be published on the agenda or announced to the membership at least 15 days prior to the meeting.

Committees that are chartered as temporary committees shall dissolve automatically upon the expiration of the certain time or occurrence of the particular event specified in their charter. All other committees shall continue in existence indefinitely unless dissolved by a majority of the votes cast at a Regular, Special, or General meeting. Working groups shall also continue in existence indefinitely unless dissolved by a majority of the votes cast at a Regular, Special, or General meeting, or by the resignation or unanimous consent of the working group’s active members.

Upon dissolution, the member(s) maintaining custody of the committee’s or working group’s records shall provide copies of those records to the Secretary.

<a name="section-8-provisional-working-groups"></a>
<a id="section-8-provisional-working-groups"></a>
## Section 8. Provisional Working Groups
Working groups may be created on a provisional basis so the chapter has a means to support nascent organizing work without a vote at a meeting. To form a provisional working group, members interested in forming the provisional working group will inform the Steering Committee of their intent to form a provisional working group, naming two provisional co-chairs and stating their purpose and immediate tasks, via a form. The provisional co-chairs must be active members as defined in these bylaws. Once the steering committee has acknowledged this form submission and verified the eligibility of the provisional co-chairs, the group is considered to be a chartered provisional working group.

After two months, the provisional working group must either submit a proposal to charter as a full working group to be voted on at the next general meeting or re-apply for a charter as a provisional working group, or else the provisional working group dissolves. After six months, a provisional working group may no longer re-apply for a charter. At any time, fifteen members can call a vote at the next general meeting, or special meeting if such action is within the purview of that meeting, on whether the provisional working group should continue to exist, or call a snap election for the provisional co-chairs at a general or special meeting.

They are not eligible to spend chapter funds without Steering Committee approval. Provisional co-chairs are not considered part of chapter leadership, and may not vouch for provisional working group members to grant them voting power.

<a name="article-viii-youth-sections"></a>
<a id="article-viii-youth-sections"></a>
# Article VIII. Youth Sections

<a name="section-1-branch-status"></a>
<a id="section-1-branch-status"></a>
## Section 1. Branch Status
Chapters of the Young Democratic Socialists located in San Francisco shall automatically be considered branches of DSA SF.

<a name="article-ix-organizing-body-and-meetings"></a>
<a id="article-ix-organizing-body-and-meetings"></a>
# Article IX. Organizing Body and Meetings

<a name="section-1-members-1"></a>
<a id="section-1-members-1"></a>
## Section 1. Members
The members of the organization (or “the membership”), meeting in General Meeting or in Special Meeting, shall be the highest body of the organization. Special Meetings are restricted in scope and shall have the authority to deal with only those specific matters for which they may be called. The Steering Committee shall be the highest body of the organization between General, Regular and Special Meetings, and shall be responsible for the administration of the organization and the implementation of policies formulated by members.

<a name="section-2-general-meetings"></a>
<a id="section-2-general-meetings"></a>
## Section 2. General Meetings
DSA SF shall hold a General Meeting annually with at least 30 days notice given to all members. The members shall meet to elect officers and to discuss and decide primarily, but not exclusively, the political orientation of the organization and program direction, including chapter priorities for the year.

Chapter Priorities are time-bound, large-scale structural and/or campaign-based initiatives that require major sustained effort and resources. The chapter will give these initiatives priority including but not limited to the following: time set aside at chapter regular meetings, space on the website, money from the budget, and mobilization of new and inactive members.

The purpose of the Chapter Priorities system as set forth in this section is to focus the chapter’s primary effort and resources on a limited number of democratically chosen collective initiatives, without precluding the chapter from working on other initiatives or projects as the need arises. Such non-priority initiatives or projects are not precluded from obtaining chapter resources such as time set aside at chapter regular meetings, space on the website, money from the budget, or mobilization of new and inactive members. In allocating limited resources such as money from the budget or mobilization capacity, the chapter, the Steering Committee, or such other body as the chapter may designate with the responsibility to allocate resources, shall first ensure that Chapter Priorities receive resources consistent with their enacting Priority Resolution(s) before allocating such resources to non-priorities. Resources that by their nature are not strictly limited, such as space on the website, shall be allocated more freely.

Priority Resolutions must contain the following:
1. A timeline for implementation and execution after the Annual General Meeting, laying out milestones for at least every quarter of the next 12 months 
2. A description of the plan beyond those 12 months 
3. A budget laying out estimated expenditures for at least every quarter 
4. A leadership structure by which chapter members in good standing can be elected as leaders responsible for the day-to-day work of implementing the priority.

Leadership for Chapter Priorities will be elected at the next chapter Regular Meeting following the meeting at which the Priority Resolutions are passed. The implementing team must provide report backs at chapter Regular Meetings and, at the end of the 12 months, must present a debrief outlining the outcomes and learnings of the project to the chapter membership during the General Meeting. Campaign leaders will bring major strategic deviations, which exceed the scope or direction laid out in the campaign proposal, to the general membership or, in between Regular meetings, to the Steering Committee.

Chapter Priorities shall be undertaken between their adoption and the following Annual General Meeting, unless an alternate timeline is approved. The Steering Committee will put out a call for proposals at least two (2) months in advance of the Annual General Meeting. Priority Resolutions can be proposed for consideration by submitting a formal proposal to the Steering Committee at least one (1) month in advance of the Annual General Meeting. The Steering Committee will make these proposals available for members to review at least two (2) weeks in advance of the Annual General Meeting and provide time on the Annual General Meeting agenda to discuss and vote on proposed Priority Resolutions.

To be considered at the Annual General Meeting, a Priority Resolution must have 15 signatories. Each eligible Priority Resolution will first go before the membership individually, requiring a majority vote in order to advance to the prioritization process. The prioritization process will be a ranked-choice election to select up to three (3) Chapter Priorities, with votes tallied by the Scottish Single Transferable Vote (STV) method. If three (3) or fewer Priority Resolutions advance to the prioritization process, all of those advanced will be automatically adopted.

Chapter Priorities can be changed, removed, and/or added at a Regular or Special meeting. A specific Chapter Priority can be removed by a majority vote of the chapter membership at a Regular, Special, or General meeting. New Priority Resolutions can be proposed for consideration by submitting a formal proposal to the Steering Committee at least one (1) month in advance of a Regular or Special meeting. The Steering Committee will make these proposals available for members to review at least two (2) weeks in advance of the meeting. The Priority Resolution must contain the same specifications required of Priority Resolutions at the General meeting. If there are fewer than three (3) Chapter Priorities, the newly proposed Priority Resolution must receive a majority of votes to be adopted. If there are already three (3) priorities, membership can replace a priority by proposing a new Priority Resolution which must receive a majority of votes to be adopted. The membership must then vote by Scottish STV to select two (2) priorities to keep among the existing three (3) priorities. Any priorities adopted this way shall be undertaken between their adoption and the following Annual General Meeting, unless an alternate timeline is approved.


<a name="section-3-special-meetings"></a>
<a id="section-3-special-meetings"></a>
## Section 3. Special Meetings
By a majority vote of the Steering Committee or a petition of fifteen members, a Special Meeting can be called, with notice given to all members at least 15 days prior to the meeting. The call to the Special Meeting shall specify the matters to be discussed therein and during the meeting, no other matter may be brought to the floor.

<a name="section-4-regular-meetings"></a>
<a id="section-4-regular-meetings"></a>
## Section 4. Regular Meetings
DSA SF shall hold regular meetings at a frequency of at least once per quarter, and up to once per month. The agenda for a Regular Meeting shall include reports from the Steering Committee and any other committee that has met since the last meeting, reports from any working groups with relevant updates, political discussion, budgetary appropriations, and education as well as open time for members to raise items not included on the meeting agenda.

<a name="section-5-voting"></a>
<a id="section-5-voting"></a>
## Section 5. Voting
Every active member in good standing of the chapter shall have voting rights at General, Special and Regular Meetings. Members shall have the ability to renew DSA dues at the start of the General Meeting.

<a name="section-6-quorum"></a>
<a id="section-6-quorum"></a>
## Section 6. Quorum
A quorum of ten percent of active membership or fifteen active members, whichever is greater, shall be required for valid General, Special and Regular Meetings.

<a name="article-x-endorsements-event-sponsorships-and-online-voting"></a>
<a id="article-x-endorsements-event-sponsorships-and-online-voting"></a>
# Article X. Endorsements, Event Sponsorships, and Online Voting
<a name="section-1-formal-endorsements-and-event-sponsorships"></a>
<a id="section-1-formal-endorsements-and-event-sponsorships"></a>
## Section 1. Formal Endorsements and Event Sponsorships
Candidates running for local, state, and national political office may be formally endorsed by DSA SF as a whole at a general, regular, or special meeting. The endorsement vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any candidate.

In addition to endorsing candidates for office, DSA SF may also choose to formally endorse particular positions on local, state, and national issues of interest to the chapter, or sponsor events, protests, or direct actions so long as the positions, events, protests, or direct actions are relevant to and consistent with the vision, mission, and objectives of DSA SF as set forth in Article II.  Such positions may be endorsed, or events, protests, or direct actions sponsored, by DSA SF as a whole at a general, regular, or special meeting. The endorsement or sponsorship vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any position or sponsor any event, protest, or direct action.

In addition, the Steering Committee has the power to sponsor events, protests, or direct actions (but not to endorse positions or candidates) at any time by a vote of two-thirds of its members. However, it may not vote to sponsor events, protests, or direct actions if doing so would conflict with or be inconsistent with a prior chapter vote, and it must report any such decisions back to the chapter as soon as reasonably possible, but by no later than at the next general or regular meeting, whichever is earliest. If the sponsored event, protest, or direct action has not yet taken place at the time of that meeting, any member in good standing may move from the floor to overturn the sponsorship by a simple majority of the votes cast.

<a name="section-2-online-voting"></a>
<a id="section-2-online-voting"></a>
## Section 2. Online Voting
The steering committee may decide, at the request of any member making such a proposal, to hold an online vote on any issue except for candidate or ballot initiative endorsements. Members shall be eligible to vote so long as they are active members in good standing, as defined in [Article III Section 4](#section-4-active), as of the start of the online voting period following close of debate. The minimum number of votes cast must be greater than or equal to the number of eligible members in attendance at whichever meeting of the last two regular chapter meetings had the largest number of eligible members in attendance, otherwise the vote will be considered invalid for lack of a quorum.
The steering committee shall announce all online votes to the chapter by email and all other reasonable means. There shall be a 72-hour period for debate following the announcement. At the close of debate, there shall be a 48-hour voting period during which any eligible member may vote. No motions to call the question or extend debate will be allowed, and no amendments will be allowed. However, at any time after the start of debate, any eligible member may motion to table the proposal with a simple majority vote.

All voting thresholds for a passing vote shall be the same as if the vote were held at an in-person meeting. Votes to abstain will count toward the quorum. A vote can become binding before the close of the 48-hour voting period if the number of votes in favor or against has surpassed the threshold that would be required if all eligible votes were cast.

<a name="section-3-ban-on-misrepresentation-of-formal-endorsements"></a>
<a id="section-3-ban-on-misrepresentation-of-formal-endorsements"></a>
## Section 3. Ban on Misrepresentation of Formal Endorsements
No officer or member of DSA SF shall publicly misrepresent the Chapter’s formal endorsements or positions.  Intentional or willful public misrepresentation of the Chapter’s endorsements or positions is grounds for suspension or expulsion under Article III Section V.

<a name="article-xi-national-delegates"></a>
<a id="article-xi-national-delegates"></a>
# Article XI. National Delegates

<a name="section-1-selection-of-delegates"></a>
<a id="section-1-selection-of-delegates"></a>
## Section 1. Selection of Delegates
Delegates to the National DSA Convention shall be elected by the membership. In accordance with the national bylaws, this election shall be held no earlier than four months, and no later than forty-five days, prior to the opening of the National Convention. No election for delegates shall be conducted before the apportionment of delegates has been received from the National DSA.

Elections shall be held, with 30 days notice, according to the procedure and rules specified in Article XII.

<a name="article-xii-elections"></a>
<a id="article-xii-elections"></a>
# Article XII. Elections

<a name="section-1-nominee-campaigns"></a>
<a id="section-1-nominee-campaigns"></a>
## Section 1. Nominee Campaigns
Campaigns for DSA SF offices shall be limited to the 30 day window between notice of the Regular Meeting and the election meeting itself. All Nominees will have equal opportunity to address the membership at the meeting.

<a name="section-2-process"></a>
<a id="section-2-process"></a>
## Section 2. Process
Elections for DSA SF leadership and delegate positions may be held at a General, Regular, or Special Meeting. All candidates shall be elected at the meeting through secret ballot by active members in good standing. Ballots must be counted immediately following the final ballot being cast. Ballots must be counted twice by a group between two and five active members in good standing in full unobstructed view of any interested Members acting as election observers. All reasonable efforts should be made to find counters that are acceptable to all candidates.

<a name="section-3-ranked-choice-voting"></a>
<a id="section-3-ranked-choice-voting"></a>
## Section 3. Ranked Choice Voting
All elections in the chapter will use ranked choice voting. "Ranked Choice Voting" means a method of casting and tabulating votes that simulates the ballot counts that would occur if all voters participated in a series of runoff elections with one candidate eliminated after each round of counting. In elections using the Ranked Choice Voting method, voters may rank the candidates in order of preference.

<a name="section-4-ballot-count"></a>
<a id="section-4-ballot-count"></a>
## Section 4. Ballot Count
Votes are tallied by the Scottish STV method, as described here. Candidates are elected according to this count procedure in the order that they receive the requisite votes.

<a name="article-xiii-dissolution"></a>
<a id="article-xiii-dissolution"></a>
# Article XIII. Dissolution
In the event of the dissolution of DSA SF, all remaining funds and assets are to be released to the nearest DSA chapter most likely to inherit its members or the DSA National when a nearby chapters cannot be found.

<a name="article-xiv-general-provisions"></a>
<a id="article-xiv-general-provisions"></a>
# Article XIV. General Provisions

<a name="section-1-interpretation"></a>
<a id="section-1-interpretation"></a>
## Section 1. Interpretation
These bylaws shall be interpreted by the Co-Chairs, subject to appeal by any member to the full Steering Committee between meetings of the chapter, or during Regular, Special, or General meetings by motion from the floor and chapter vote. All powers not delegated in these bylaws or by vote of the membership are reserved to the membership.

<a name="section-2-rules"></a>
<a id="section-2-rules"></a>
## Section 2. Rules
The rules contained in the current edition of Robert’s Rules of Order Newly Revised shall govern the organization in all cases to which they are applicable and in which they are not inconsistent with these Bylaws or standing rules of DSA SF, except that the chapter or a committee may vote to use modified or alternative rules of procedure in their meetings so long as they are not inconsistent with these Bylaws.

<a name="article-xv-amendments"></a>
<a id="article-xv-amendments"></a>
# Article XV. Amendments

<a name="section-1-qualifying-proposals"></a>
<a id="section-1-qualifying-proposals"></a>
## Section 1. Qualifying Proposals
These Bylaws may be amended only on written proposal of at least fifteen co-sponsors who are active members in good standing, or of 10% of active members in good standing, whichever is less. Proposals must identify both the proposed language of the amendment and the current language in the bylaws to be deleted, replaced, or amended.

Proposals must be submitted in writing to the Steering Committee, which as soon as feasible shall circulate all proposals that satisfy the sponsorship and identification requirements of this section to the membership for review.

<a name="section-2-introduction-and-ratification"></a>
<a id="section-2-introduction-and-ratification"></a>
## Section 2. Introduction and Ratification
The co-sponsors shall introduce their proposals to the chapter at the first Regular or General Meeting, or Special Meeting called for that purpose, taking place at least two weeks after the proposals are circulated to the membership. The proposals shall not be voted on during the meeting.

No later than one week after the meeting, the proposals’ co-sponsors shall inform the Steering Committee in writing whether they choose to withdraw the proposals, make any changes to the proposals they deem necessary, or reaffirm that they wish to proceed with the proposals as introduced. Unless withdrawn, the Steering Committee shall then recirculate the proposals to the chapter as soon as feasible.

The chapter will vote whether to ratify the proposals at the next Regular or General Meeting, or Special Meeting called for that purpose, taking place at least two weeks after the proposals are recirculated to the chapter. Ratification requires a two-thirds vote of members present.

<a name="article-xvi-grievance-officers"></a>
<a id="article-xvi-grievance-officers"></a>
# Article XVI. Grievance Officers

<a name="section-1-proposals"></a>
<a id="section-1-definition-and-scope"></a>
## Section 1. Definition and Scope

**Definition.** The Grievance Officers (“GOs”) will be two members elected by the chapter to hear and provide recommendations on grievances against chapter members. The two GOs will be elected simultaneously using the same method as used to elect the Steering Committee. The term of the GOs will be one year or until successors are elected.

**Scope.** The complaints that the GOs are empowered to address will encompass the following:

 1. Complaints regarding harassment on the basis of sex, gender, gender identity or expression, sexual orientation, physical appearance, disability, race, color, religion, national origin, class, age, or profession. Harassing or abusive behavior, such as unwelcome (in the sense that the complainant did not solicit or incite it, and found it undesirable or offensive) attention, inappropriate or offensive remarks, slurs, or jokes, physical or verbal intimidation, stalking, inappropriate physical contact or proximity, and other verbal and physical conduct constitute harassment when:
    1. Submission to such conduct is made either explicitly or implicitly a term or condition of a member’s continued affiliation with DSA;
    2. Submission or rejection of such conduct by an individual is used as the basis for organizational decisions affecting such individual; or
    3. Such conduct has the purpose or effect of creating a hostile environment interfering with an individual’s capacity to organize within DSA. A “hostile environment” is one in which the harassment is sufficiently severe or pervasive as to alter the conditions of membership and create an abusive organizing environment. Whether harassment is severe enough to reach this level is determined by whether a reasonable person in the complainant’s position (“position” being defined broadly to include whether, for example, the complainant is a member of a group that is often marginalized or discriminated against) would be offended by the conduct.
 2. Complaints regarding alleged violations of the DSA SF Code of Conduct other than:
    1. Those relating to claiming to speak on behalf of or represent the chapter without its approval, and
    2. Those relating to a candidate’s advertising or publicizing of their member status in a manner designed to promote their candidacy.

<a name="section-2-grievance-officer-role-and-responsibilities"></a>
<a id="section-2-grievance-officer-role-and-responsibilities"></a>
## Section 2. Grievance Officer Role and Responsibilities

**Role.** The GOs will follow the process laid out in this section upon receipt of a written complaint submitted to them by a complainant.

**Process.** After a written complaint has been received by the GOs:

 1. The GOs will contact the complainant to acknowledge receipt;
 2. The GOs will contact the accused member within seven days to notify them that a complaint has been filed against them and request a written response to the complaint either affirming or denying its substance to be submitted within seven days of being notified;
 3. The GOs will as soon as possible (and at any time during the grievance process) make a joint recommendation to the Steering Committee on whether immediate action (such as a temporary suspension or an instruction to the accused not to contact the complainant) is necessary to preserve the safety of the complainant and/or any other members, and/or whether the Steering Committee should retain legal or other professional help to assist in the response to the complaint;
 4. Upon receipt of the accused’s response or if the accused fails to respond within seven days, the GO(s) responsible for adjudicating the dispute will determine whether the complaint is credible and, if necessary, make a recommendation to the Steering Committee of appropriate disciplinary or remedial action as soon as practicable, but ultimately within thirty days of the complaint being filed. The GO(s) may notify the Steering Committee of the complaint and its substance at any time after the complaint is filed, but must give written notice to both the complainant and the accused member before doing so.
 5. In determining whether a complaint is “credible,” the following standards shall apply. A factual allegation in a complaint is "credible" if it more-likely-than-not occurred. An allegation in a complaint that particular behavior constituted harassment (for example, if there is a dispute about whether a particular statement or comment had the intent or effect of causing a hostile environment) is "credible" if a reasonable person in the complainant's position would agree that the behavior constituted harassment.
 6. In determining whether the complaint is credible, the GOs overseeing the dispute should, to the extent necessary and possible, investigate the complaint by:
    1. Interviewing other members with direct knowledge of the substance of the report;
    2. Requesting documentation from either the accuser or accused or any other parties directly involved; and
    3. Employing any other legal means, with the utmost respect for the confidentiality of the parties. If the accused refuses to submit a written response within seven days, the GOs may (but are not required to) consider the refusal to respond as evidence that the complaint is credible.
 7. In the case of a complaint made against the Steering Committee or any of its members, the GOs may, at their discretion and subject to the consent of the complainant(s), publish their recommendations for disciplinary or remedial action publicly to chapter members (removing all identifying information of involved parties except the Steering Committee, unless asked not to), and, no more than thirty days later, report publicly on whether the recommendations were adhered to.
 8. In the case of a complaint alleging retaliation against a complainant, which could include threats, intimidation, reprisals and/or adverse actions related to organizing, the GOs will determine whether to consolidate the retaliation into the original complaint, or to treat it as a separate complaint.

<a id="disciplinary-and-remedial-recommendations"></a>
### Disciplinary and Remedial Recommendations

In the event that the GOs determine a complaint is not credible, they may recommend either that the Steering Committee take no action on the complaint or that it request the parties undergo voluntary, informal conflict resolution. In the event that the GOs determine that a complaint is credible, they may recommend from among the below disciplinary or remedial actions. The recommended action should be commensurate with the severity and pervasiveness of the offense.
 1. Expulsion from DSA SF;
 2. Temporary or indefinite suspension and/or removal of the accused from some or all DSA SF activities, events, spaces, committees, and communication channels;
 3. Formal notice of expected changes in behavior;
 4. Voluntary, informal mediation or conflict resolution;
 5. No action; OR
 6. Other disciplinary or remedial measures as necessary to protect the complainant and/or other members from an unsafe space or to repair harm done to individuals, relationships and/or the organization.

<a id="other-responsibilities"></a>
### Other Responsibilities

The GOs will also:
 1. Archive all complaints, responses of accused members, investigatory materials, and recommendations for at least one year after the closing of the grievance process.
 2. Organize a once-yearly training on harassment and Code of Conduct violations, or ensure another member does so.
 3. “Compile a yearly report that details:
    1. How many complaints were made and how many disciplinary actions were taken in the preceding calendar year; and
    2. Any recommended changes for making the reporting system more effective. The report will not include personally identifying information of any parties in any dispute.
 4. The local GOs will send the yearly report to the national Harassment GO(s) as soon as possible after January 1 of each year.

<a id="consensus-and-recusal"></a>
### Consensus and Recusal

**Consensus.** When addressing a complaint, GOs must act through consensus whenever possible. When this is not possible, each GO will prepare a proposal for the Steering Committee containing recommendations for action. The Steering Committee may choose to utilize parts of each proposal or to adopt one or the other.

**Recusal.** Any GO who is personally involved in the issues raised in the Grievance must recuse themself. GOs may also voluntarily recuse themselves in other situations where they do not believe they can fairly investigate the grievance.

A complainant or accused party may also request that a GO recuse themself from a case if they feel there will not be a fair investigation, in which case that GO must recuse themself.
In the event that all GOs recuse themselves, the Steering Committee shall appoint a non-conflicted member who is not a Steering Committee member to temporarily take on the GO role to handle the grievance. The Steering Committee may also require the temporary GO to enlist a professional.

<a name="section-3-candidacy-and-elections"></a>
<a id="section-3-candidacy-and-elections"></a>
## Section 3. Candidacy and Elections

Any active member in good standing of the chapter may run to be a GO.
GOs will be elected through the same process as the Steering Committee, including the requirements for nomination and advanced notice for election.
Upon election, each Grievance Officer will receive training.

<a name="section-4-vacancies"></a>
<a id="section-4-vacancies"></a>
## Section 4. Vacancies

Should a grievance officer leave DSA SF, resign the position, or be removed from office, the position will be left vacant until an election can be conducted. This must occur within 60 days after the vacancy and can take place at a Regular or General meeting, or a Special Meeting called for that purpose.

<a name="section-5-removal-of-a-go"></a>
<a id="section-5-removal-of-a-go"></a>
## Section 5. Removal of a GO

Grievance officers may be removed by a referendum vote of the Local’s members, by the same procedure required for the removal of Officers.
